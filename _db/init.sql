DROP DATABASE IF EXISTS tamojunto;
CREATE DATABASE tamojunto;
DROP USER IF EXISTS tamojunto;
CREATE USER tamojunto
WITH PASSWORD 'tamojunto';
GRANT ALL PRIVILEGES ON DATABASE "tamojunto" to tamojunto;
SET TIMEZONE TO 'America/Sao_Paulo';
