package com.contrafluxo.tamojunto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@SpringBootApplication
public class TamojuntoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TamojuntoApplication.class, args);
	}

	@Bean
	public LocaleResolver localeResolver() {
		//forçando pt_Br, se for ter mais de um locale, usar o AcceptHeaderLocaleResolver para resolver o Header Accept-Language,
		//ou resolver o locale de acordo com alguma preferencia salva
		return new LocaleResolver() {
			@Override
			public Locale resolveLocale(HttpServletRequest request) {
				return  new Locale("pt", "BR");
			}

			@Override
			public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
				LocaleContextHolder.setLocale(locale);
			}
		};

	}

}
