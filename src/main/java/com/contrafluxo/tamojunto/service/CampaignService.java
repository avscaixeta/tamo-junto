package com.contrafluxo.tamojunto.service;

import com.contrafluxo.tamojunto.data.CampaignRepository;
import com.contrafluxo.tamojunto.data.entity.Campaign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

@Service
public class CampaignService implements Serializable {

    private final CampaignRepository campaignRepository;

    @Autowired
    public CampaignService(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }

    @Transactional
    public void saveDocuments(MultipartFile file) {
        try {
            Campaign campaign = new Campaign();
            campaign.setCreatedAt(Calendar.getInstance());
            campaign.setUpdatedAt(Calendar.getInstance());
            campaign.setFileName(file.getName());
            campaign.setFile(file.getBytes());
            campaign.setType("SECURITY");
            campaignRepository.save(campaign);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public Campaign findByFileName(String fileName) {
        return campaignRepository.findByFileName(fileName);
    }

}
