package com.contrafluxo.tamojunto.service;


import com.contrafluxo.tamojunto.data.entity.User;
import com.contrafluxo.tamojunto.security.authentication.UserPrincipal;
import com.contrafluxo.tamojunto.vo.CredentialsVO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;


/**
 * Controls the user auth, issues access tokens, delegate customer creation accounts, etc.
 */
@Service
@Validated
public class SecurityService {

    @Value("app.name")
    private String appName;

    @Value("jwt.secret")
    private String secret;

//    @Value("${jwt.expires_in}")
//    private int expiresIn;

    private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    private final UserService userService;
//    private final CompanyService companyService;

    @Autowired
    public SecurityService(UserService userService) {
        this.userService = userService;
    }

    /*
     * Private members
     */

    private Claims getClaimsFromToken(@NotBlank String token) {
        Claims claims = null;

        try {
            claims = Jwts.parser()
                    .setSigningKey(this.secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception ignored) {
        }

        return claims;
    }

    private String generateJtwToken(@NotNull User user) {
        JwtBuilder jwt = Jwts.builder()
                .setIssuer(appName)
                .setSubject(user.getId())
//                .claim("individualId", user.getCompanyIndividual().getIndividualId())
//                .claim("name", user.getCompanyIndividual().getIndividual().getName())
                .claim("email", user.getEmail());
//                .claim("roles", user.getGroup().getRoles())
//                .claim("credentialsVersion", user.getCredentialsVersion());

//        if (user.getCompanyIndividual().getCompanyId() != null) {
//            jwt.claim("companyId", user.getCompanyIndividual().getCompanyId());
//            jwt.claim("companyStatus", user.getCompanyIndividual().getCompany().getCompanyStatus());
//        }

        jwt.setIssuedAt(Calendar.getInstance().getTime())
//                .setExpiration(new Date(System.currentTimeMillis() + this.expiresIn))
                .setExpiration(new Date(System.currentTimeMillis() + 3600000))
                .signWith(SIGNATURE_ALGORITHM, secret);


        return jwt.compact();
    }

    private String getUserIdFromToken(@NotBlank String token) {
        String userid = null;
        final Claims claims = this.getClaimsFromToken(token);

        if (claims != null)
            userid = claims.getSubject();

        return userid;
    }

    private String getClaimFromToken(@NotBlank String token, @NotBlank String key) {
        String value = null;
        final Claims claims = this.getClaimsFromToken(token);

        if (claims != null)
            value = claims.get(key) != null ? claims.get(key).toString() : null;

        return value;
    }


    /*
     * Public members
     */
    public UserPrincipal createStatelessSession(@NotNull User user) {
        String token = this.generateJtwToken(user);
//        return new UserPrincipal(token, user, user.getGroup().getRoles());
        return new UserPrincipal(token, user);
    }

    public UserPrincipal authenticate(@Valid @NotNull CredentialsVO credentials) {
        User user = this.userService.loginUserByEmail(credentials.getEmail());

//        Assert.isNotNull(user, E_INVALID_CREDENTIALS);
//        Assert.isFalse(user.getStatus().equals(WAITING_PRE_APPROVAL), E_USER_WAITING_PRE_APPROVAL);
//        Assert.isFalse(user.getStatus().equals(LOCKED), E_USER_IS_LOCKED);
//        Assert.isTrue(user.getStatus().equals(ENABLED), E_ACCOUNT_NOT_ENABLED);

//        userService.ensureUserIsAuthorized(user, credentials);
//
//        if (!user.getGroup().isBackofficeGroup())
//            companyService.ensureCompanyIsAuthorized(user.getCompanyIndividual().getCompanyId());

        return createStatelessSession(user);
    }

    public UserPrincipal authenticate(@NotBlank String token) throws BadCredentialsException {
        String userId = getUserIdFromToken(token);
        String credentialsVersion = getClaimFromToken(token, "credentialsVersion");

        UserPrincipal principal;

//        if (userId == null)
//            throw new BadCredentialsException(MessageCode.E_SESSION_EXPIRED.name());

        User user = userService.loginUserById(userId);

//        if (user == null)
//            throw new BadCredentialsException(MessageCode.E_INVALID_ACCOUNT.name());
//
//        else if (!user.getCredentialsVersion().equals(credentialsVersion))
//            throw new BadCredentialsException(MessageCode.W_CREDENTIALS_HAS_BEEN_CHANGED.name());
//
//        if (!user.getGroup().isBackofficeGroup())
//            companyService.ensureCompanyIsAuthorized(user.getCompanyIndividual().getCompanyId());

        principal = createStatelessSession(user);

        return principal;
    }
}