package com.contrafluxo.tamojunto.service;

import com.contrafluxo.tamojunto.data.NewsRepository;
import com.contrafluxo.tamojunto.data.entity.News;
import com.contrafluxo.tamojunto.vo.NewsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

@Service
public class NewsService implements Serializable {

    private final NewsRepository newsRepository;

    @Autowired
    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Transactional
    public List<News> findAll() {
        return newsRepository.findAll();
    }

    @Transactional
    public News saveNews(NewsVO newsVO) {
        News news = new News();
        news.setUrl(newsVO.getUrl());
        news.setCreatedAt(Calendar.getInstance());
        news.setUpdatedAt(Calendar.getInstance());
        news.setStatus("ATIVO");

        return newsRepository.save(news);
    }

    @Transactional
    public void deleteNews(Long newsId) {
        newsRepository.deleteById(newsId);
    }
}
