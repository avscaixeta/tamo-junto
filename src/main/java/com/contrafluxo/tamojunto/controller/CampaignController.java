package com.contrafluxo.tamojunto.controller;

import com.contrafluxo.tamojunto.data.entity.Campaign;
import com.contrafluxo.tamojunto.service.CampaignService;
import com.contrafluxo.tamojunto.util.ResponseUtils;
import com.contrafluxo.tamojunto.vo.EnvelopeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping(value = "/rest/v1/campaign")
public class CampaignController {

    private final CampaignService campaignService;

    @Autowired
    public CampaignController(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity uploadFile(@RequestParam MultipartFile file) {
        System.out.println("----------------------------------------------");
        campaignService.saveDocuments(file);
        return null;
    }

}

