package com.contrafluxo.tamojunto.controller;

import com.contrafluxo.tamojunto.data.enumeration.MessageCode;
import com.contrafluxo.tamojunto.service.NewsService;
import com.contrafluxo.tamojunto.util.ResponseUtils;
import com.contrafluxo.tamojunto.vo.EnvelopeVO;
import com.contrafluxo.tamojunto.vo.NewsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "/rest/v1/news")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<EnvelopeVO> findAll() {
        return ResponseUtils.ok(newsService.findAll());
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<EnvelopeVO> saveNews(@RequestBody @Valid @NotNull NewsVO news) {
        return ResponseUtils.ok(newsService.saveNews(news), MessageCode.I_THE_X_Y_HAS_BEEN_Z);
    }

    @DeleteMapping()
    public ResponseEntity<EnvelopeVO> deleteNews(@RequestParam(value = "id") Long id) {
        newsService.deleteNews(id);
        return ResponseUtils.ok(MessageCode.I_THE_X_Y_HAS_BEEN_Z);
    }
}

