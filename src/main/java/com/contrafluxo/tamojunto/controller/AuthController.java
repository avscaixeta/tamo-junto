package com.contrafluxo.tamojunto.controller;


import com.contrafluxo.tamojunto.security.authentication.UserPrincipal;
import com.contrafluxo.tamojunto.service.SecurityService;
import com.contrafluxo.tamojunto.util.Json;
import com.contrafluxo.tamojunto.util.ResponseUtils;
import com.contrafluxo.tamojunto.vo.CredentialsVO;
import com.contrafluxo.tamojunto.vo.EnvelopeVO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "/rest/v1/auth", produces = "application/json")
public class AuthController {

    SecurityService securityService;

    public AuthController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
    public ResponseEntity<EnvelopeVO> login(@Valid @NotNull @RequestBody CredentialsVO credentials) {

        UserPrincipal principal = securityService.authenticate(credentials);

//        UserVO userVo = UserMapper.INST.userToUserVO(principal.getUser());

        return ResponseUtils.ok(Json.inst()
                .kv("token", principal.getToken())
                .kv("user", principal)
                .toMap());
    }

}

