package com.contrafluxo.tamojunto.security;


import com.contrafluxo.tamojunto.security.filter.CorsFilter;
import com.contrafluxo.tamojunto.security.filter.JwtFilter;
import com.contrafluxo.tamojunto.security.jwt.JwtAuthorizationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthorizationEntryPoint jwtAuthorizationEntryPoint;

    @Bean
    public JwtFilter jwtAuthenticationTokenFilter() throws Exception {
        return new JwtFilter();
    }

    @Bean
    public CorsFilter securityRequestFilter() throws Exception {
        return new CorsFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .exceptionHandling().authenticationEntryPoint(jwtAuthorizationEntryPoint).and()
                .addFilterBefore(securityRequestFilter(), ChannelProcessingFilter.class)
                .addFilterBefore(jwtAuthenticationTokenFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests().anyRequest().permitAll();

    }

//    @Autowired
//    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
//
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("{noop}password");
//
//        auth.inMemoryAuthentication()
//                .withUser("user")
//                .password("{noop}password")
//                .authorities("USER");
//    }
}
