package com.contrafluxo.tamojunto.security.authentication;


import com.contrafluxo.tamojunto.data.entity.User;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;


public class UserPrincipal implements UserDetails {

    @Getter
    private User user;

    @Getter
    private Collection<? extends GrantedAuthority> authorities;

    @Getter
    private String token;

    /*
     * Constructor
     */
    public UserPrincipal() {
    }

    public UserPrincipal(String token,
                         User user,
                         Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.user = user;
        this.authorities = authorities != null
                ? Collections.unmodifiableCollection(authorities)
                : Collections.emptyList();
    }

    public UserPrincipal(String token,
                         User user) {
        this.token = token;
        this.user = user;
        this.authorities = authorities != null
                ? Collections.unmodifiableCollection(authorities)
                : Collections.emptyList();
    }

    /*
     * Public Methods
     */

//    public User getUser() {
//        return this.user;
//    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    public String getName() {
        return user.getId();
    }

    @Override
    public boolean isEnabled() {
        //return user.getStatus().equals(UserStatus.ENABLED);
        return true;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }
}
