package com.contrafluxo.tamojunto.security.filter;

import com.contrafluxo.tamojunto.data.enumeration.MessageCode;
import com.contrafluxo.tamojunto.security.authentication.UserPrincipal;
import com.contrafluxo.tamojunto.security.jwt.JwtTokenAuthentication;
import com.contrafluxo.tamojunto.service.SecurityService;
import com.contrafluxo.tamojunto.vo.EnvelopeVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JwtFilter extends OncePerRequestFilter {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Value(value = "jwt.header")
    private String AUTH_HEADER = "Authorization";

    @Autowired
    private SecurityService securityService;

    @Autowired
    private ObjectMapper objectMapper;

    private String getToken(HttpServletRequest request) {
        String authHeader = request.getHeader(AUTH_HEADER);

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.replace("Bearer ", "");
        }

        return null;
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String authToken = getToken(request);
        // get username from token
        UserPrincipal principal;

        if (StringUtils.isNotBlank(authToken)) {

            try {
                principal = securityService.authenticate(authToken);
                SecurityContextHolder.getContext().setAuthentication(new JwtTokenAuthentication(principal));
                response.addHeader("Renew-JWT", principal.getToken());

            } catch (BadCredentialsException be) {
                EnvelopeVO envelopeDto = EnvelopeVO.instance().message(MessageCode.valueOf(be.getMessage()));
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.setContentType("application/json");
                response.getWriter().write(objectMapper.writeValueAsString(envelopeDto));
                response.getWriter().flush();
                response.getWriter().close();

                return;
            }
        }

        chain.doFilter(request, response);
    }
}
