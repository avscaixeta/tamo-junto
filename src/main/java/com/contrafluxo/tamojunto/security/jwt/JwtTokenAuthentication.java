package com.contrafluxo.tamojunto.security.jwt;

import com.contrafluxo.tamojunto.security.authentication.UserPrincipal;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;


public class JwtTokenAuthentication extends AbstractAuthenticationToken {
    private final UserPrincipal user;

    public JwtTokenAuthentication(UserPrincipal user) {
        super(user.getAuthorities());
        this.user = user;
        this.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return user.getToken();
    }

    @Override
    public UserDetails getPrincipal() {
//        return user;
        return null;
    }
}
