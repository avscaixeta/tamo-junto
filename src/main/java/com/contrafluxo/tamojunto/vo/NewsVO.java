package com.contrafluxo.tamojunto.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;

@Getter
@Setter
public class NewsVO {

    private Long id;

    private String url;

    private String status;

    private Calendar createdAt;

    private Calendar updatedAt;
}
