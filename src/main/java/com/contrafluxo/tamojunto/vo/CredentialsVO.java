package com.contrafluxo.tamojunto.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CredentialsVO {

    private String email;

    private String username;

    private String password;

    private String token;
}

