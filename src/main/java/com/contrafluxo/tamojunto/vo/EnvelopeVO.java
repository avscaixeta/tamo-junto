package com.contrafluxo.tamojunto.vo;

import com.contrafluxo.tamojunto.data.enumeration.MessageCode;
import com.contrafluxo.tamojunto.util.ApplicationContextUtil;
import com.contrafluxo.tamojunto.util.I18N;
import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Simples VO para envelopar as respostas do sistema sempre em um formato padrão.
 */
@Getter
public class EnvelopeVO implements Serializable {

    private List<MessageVO> messages;
    private Object data;

    private EnvelopeVO() {
        messages = new ArrayList<>();
    }

    public static EnvelopeVO instance() {
        return new EnvelopeVO();
    }

    private String translate(MessageCode messageCode, Object... params) {
        //FIXME Verificar o motivo do método translate não estar conseguindo pegar o "locale" PT_BR
        return ApplicationContextUtil.getBean(I18N.class).getString(new Locale("pt", "BR"), messageCode, params);
    }

    public EnvelopeVO message(MessageCode messageCode, Object... params) {
        if (messageCode != null)
            this.getMessages().add(MessageVO.instance(messageCode.getSeverity(), messageCode.name(), translate(messageCode, params), params));

        return this;
    }

    public EnvelopeVO data(Object object) {
        if (this.data != null)
            throw new RuntimeException("Data object already informed: " + this.data);

        this.data = object;
        return this;
    }
}

