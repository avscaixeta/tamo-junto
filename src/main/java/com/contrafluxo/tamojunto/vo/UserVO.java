package com.contrafluxo.tamojunto.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UserVO implements Serializable {
    boolean enabled;
    boolean firstAccess;
    private String userId;
    private Long individualId;
    private Long companyId;
    private String companyStatus;
    private String name;
    private String cpf;
    private String rg;
    private String pictureUrl;

    private String email;

    public UserVO() {
        super();
    }
}

