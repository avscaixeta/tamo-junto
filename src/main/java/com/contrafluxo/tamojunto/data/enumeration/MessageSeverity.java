package com.contrafluxo.tamojunto.data.enumeration;

public enum MessageSeverity {
    SUCCESS,
    INFO,
    WARNING,
    ERROR
}

