package com.contrafluxo.tamojunto.data.enumeration;

import lombok.Getter;

@Getter
public enum MessageCode {

    I_THE_X_Y_HAS_BEEN_Z(200, MessageSeverity.SUCCESS);

    private Integer code;
    private MessageSeverity severity;

    MessageCode(int code, MessageSeverity severity) {
        this.code = code;
        this.severity = severity;
    }

}
