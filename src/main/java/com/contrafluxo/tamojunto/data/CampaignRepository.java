package com.contrafluxo.tamojunto.data;

import com.contrafluxo.tamojunto.data.entity.Campaign;
import com.contrafluxo.tamojunto.data.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long>, PagingAndSortingRepository<Campaign, Long>, QuerydslPredicateExecutor<Campaign> {

    Campaign findByFileName(@Param("fileName") String fileName);
}
