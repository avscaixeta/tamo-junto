package com.contrafluxo.tamojunto.data;

import com.contrafluxo.tamojunto.data.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepository extends JpaRepository<News, Long>, PagingAndSortingRepository<News, Long>, QuerydslPredicateExecutor<News> {


}
