package com.contrafluxo.tamojunto.data.entity;


import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Map;

@Entity
@Table(name = "campaign")
@Getter
@Setter
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
public class Campaign { //extends BaseEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_campaign")
    @SequenceGenerator(allocationSize = 1, name="seq_campaign", sequenceName="seq_campaign")
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(name = "file")
    private byte[] file;

    @Column
    private String fileName;

    @Column
    private String type;



//    @Type(type = "jsonb")
//    @Column(name = "security_documents", columnDefinition = "jsonb")
//    private Map<String, Map<String, String>> security_documents;
//
//    @Type(type = "jsonb")
//    @Column(name = "education_documents", columnDefinition = "jsonb")
//    private Map<String, Map<String, String>> education_documents;
//
//    @Type(type = "jsonb")
//    @Column(name = "infrastructure_documents", columnDefinition = "jsonb")
//    private Map<String, Map<String, String>> infrastructure_documents;
//
//    @Type(type = "jsonb")
//    @Column(name = "invitation_documents", columnDefinition = "jsonb")
//    private Map<String, Map<String, String>> invitation_documents;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdAt", nullable = false, updatable = false)
    private Calendar createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedAt", nullable = false)
    private Calendar updatedAt;


}

