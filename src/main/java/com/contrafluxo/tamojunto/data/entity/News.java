package com.contrafluxo.tamojunto.data.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "news")
@Getter
@Setter
public class News { //extends BaseEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_news")
    @SequenceGenerator(allocationSize = 1, name="seq_news", sequenceName="seq_news")
    @Column(name = "id", insertable = false, nullable = false)
    private Long id;

    @Column(length = 100)
    private String url;

    @Column(length = 30)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdAt", nullable = false, updatable = false)
    private Calendar createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedAt", nullable = false)
    private Calendar updatedAt;

    public News() {
        super();
    }

    public News(Long id) {
        this();
        this.id = id;
    }
}

