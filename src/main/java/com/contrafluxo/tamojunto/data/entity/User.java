package com.contrafluxo.tamojunto.data.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email", name = "uk_email"),
})
@Getter
@Setter
public class User { //extends BaseEntity {

    @Id
    @GeneratedValue(generator = "user-uuid")
    @GenericGenerator(name = "user-uuid", strategy = "uuid2")
    @Column(length = 36, nullable = false, unique = true, updatable = false)
    private String id;

    @Email
    @NotBlank
    @Length(max = 100)
    @Column(length = 100, unique = true, nullable = false)
    private String email;

    @Length(max = 100)
    @Column(length = 100)
    private String password;

    public User() {
        super();
    }

    public User(String id) {
        this();
        this.id = id;
    }
}
