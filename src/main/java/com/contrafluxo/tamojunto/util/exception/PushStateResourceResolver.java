package com.contrafluxo.tamojunto.util.exception;

import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.resource.ResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Redirects every page to index.html
 * Used to handle the router
 */
public class PushStateResourceResolver implements ResourceResolver {
    private Resource index;
    private List<String> handledExtensions = Arrays.asList("html", "js", "json", "css", "eot", "ttf", "woff", "woff2", "appcache", "jpg", "jpeg", "gif", "ico", "png", "svg", "docx");
    private List<String> ignoredPaths = Arrays.asList("/rest/v1");

    public PushStateResourceResolver(String targetIndex) {
        index = new ClassPathResource(targetIndex);
    }

    private Resource resolve(String requestPath, List<? extends Resource> locations) {
        String extension = StringUtils.getFilenameExtension(requestPath);

        boolean isHandled = handledExtensions.stream().anyMatch(ext -> ext.equals(extension));

        if (ignoredPaths.contains(requestPath))
            return null;

        if (isHandled)
            return locations.stream()
                    .map(loc -> createRelative(loc, requestPath))
                    .filter(res -> res != null && res.exists())
                    .findFirst()
                    .orElse(null);

        return index;
    }

    @SneakyThrows
    private Resource createRelative(Resource resource, String relativePath) {
//        String realPath = relativePath.contains("assets/")
//                ? relativePath.substring(relativePath.indexOf("assets/"))
//                : relativePath;

        return resource.createRelative(relativePath);
    }


    /*
     * Public members
     */

    @Override
    public Resource resolveResource(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain) {
        return resolve(requestPath, locations);
    }

    @Override
    public String resolveUrlPath(String resourcePath, List<? extends Resource> locations, ResourceResolverChain chain) {
        Resource resolvedResource = resolve(resourcePath, locations);

        if (resolvedResource == null)
            return null;

        try {
            return resolvedResource.getURL().toString();
        } catch (IOException e) {
            return resolvedResource.getFilename();
        }
    }
}
